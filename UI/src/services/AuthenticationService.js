import Api from './Api'
import $store from '../store/index'

/**
 ******************************
 * @API
 ******************************
*/

export default {
  async login (req, res) {
    const Users = $store.state.users
    const user = Users.find((user) => {
      return user.username === (req.username) && user.password === (req.password)
    })
    if (user) {
      const response = await Api().post('/login', user)
      $store.dispatch('setUser', response.data.user)
      $store.dispatch('setToken', response.data.token.token)
      $store.dispatch('setTokenExpDate', response.data.token.expires_in)
      $store.dispatch('setRefreshToken', response.data.token.refreshToken)
    }
  },
  async signup (user) {
    $store.dispatch('newUser', user)
    this.login(user)
  }
}

export async function refreshTokens () {
  const refreshToken = $store.state.refreshToken
  const res = await Api().post('/refreshtoken', { refreshToken })
  if (res.status === 200) {
    $store.dispatch('setToken', res.data.token.token)
    $store.dispatch('setRefreshToken', res.data.token.refreshToken)
    $store.dispatch('setTokenExpDate', res.data.token.expires_in)
  }
}

/**
 ******************************
 * @methods
 ******************************
*/

export function isAccessTokenExpired () {
  const accessTokenExpDate = $store.state.accessTokenExpDate
  const nowTime = Math.floor(new Date().getTime() / 1000)
  return accessTokenExpDate <= nowTime
}

export function getRefreshToken () {
  return $store.state.refreshToken
}
