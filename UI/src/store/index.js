import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: true,
  plugins: [
    createPersistedState()
  ],
  state: {
    token: null,
    refreshToken: null,
    isUserLoggedIn: false,
    accessTokenExpDate: '',
    user: null,
    users: []
  },
  mutations: {
    setToken (state, token) {
      state.token = token
      state.isUserLoggedIn = !!(token)
    },
    setRefreshToken (state, refreshToken) {
      state.refreshToken = refreshToken
    },
    setUser (state, user) {
      state.user = user
    },
    setUsers (state, user) {
      state.users = user
    },
    newUser (state, user) {
      state.users.push(user)
    },
    setTokenExpDate (state, expDate) {
      state.accessTokenExpDate = expDate
    }
  },
  actions: {
    setToken ({ commit }, token) {
      commit('setToken', token)
    },
    setTokenExpDate ({ commit }, expDate) {
      commit('setTokenExpDate', expDate)
    },
    setRefreshToken ({ commit }, refreshToken) {
      commit('setRefreshToken', refreshToken)
    },
    setUser ({ commit }, user) {
      commit('setUser', user)
    },
    setUsers ({ commit }, user) {
      commit('setUsers', user)
    },
    newUser ({ commit }, user) {
      commit('newUser', user)
    }
  },
  getters: {
    users (state) {
      return state.users
    },
    user (state) {
      return state.user
    }
  }
})
