import * as authService from '../services/AuthenticationService'

export default function initCurrentUserStateMiddleware (to, from, next) {
  if (authService.isAccessTokenExpired() && authService.getRefreshToken()) {
    return authService.refreshTokens()
      .then(() => next())
      .catch(error => console.log(error))
  }
  next()
}
