module.exports = {
    authentication: {
      jwtSecret: 'secret-token',
      refreshTokenSecret: "secret-refresh-token"
    }
}