const jwt = require('jsonwebtoken');
var jwt_decode = require('jwt-decode');
const config = require('../config/config')

function jwtSignUser (user) {
  const expires_in = Math.floor(Date.now() / 1000) + 180;
  const expires_in_refreshToken = 60 * 60 * 24 * 7;
  const token = jwt.sign(user, config.authentication.jwtSecret, {
    expiresIn: expires_in
  })
  const refreshToken = jwt.sign(user, config.authentication.refreshTokenSecret, {
    expiresIn: expires_in_refreshToken
  })
  
  return { token, refreshToken, expires_in}
}
function refreshToken (userToken) {
  var decoded = jwt_decode(userToken);
  const user = {
    username: decoded.username,
    password: decoded.password,
    role: decoded.role
  }
  const expires_in = Math.floor(Date.now() / 1000) + 180;
  const expires_in_refreshToken = 60 * 60 * 24 * 7;
  const token = jwt.sign(user, config.authentication.jwtSecret, {
    expiresIn: expires_in
  })
  const refreshToken = jwt.sign(user, config.authentication.refreshTokenSecret, {
    expiresIn: expires_in_refreshToken
  })

  return { token, refreshToken, expires_in }
}

module.exports = {
    jwtSignUser,
    refreshToken
};