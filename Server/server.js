const express = require('express');
const bodyParser = require('body-parser');


const AuthenticationController = require('./controllers/AuthenticationController');
const RefreshTokenController = require('./controllers/RefreshTokenController');

const app = express();
app.use(bodyParser.json());

app.use(express.json());

app.post('/login', AuthenticationController.login);
app.post('/refreshtoken', RefreshTokenController.refreshTokens);

app.listen(8080, () => {
    console.log('API app started');
})