const jwt = require('../config/jwtSign');

module.exports = {
    refreshTokens (req, res) {
        try {
            const user = req.body.refreshToken;
            res.status(200).send({
                user,
                token: jwt.refreshToken(user)
            });
            } catch (err) {
            res.status(500).send({
                error: 'Произошла ошибка при попытке войти'
            })
        }
    }
}
