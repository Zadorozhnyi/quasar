const jwt = require('../config/jwtSign');

module.exports = {
  login (req, res) {
    try {
      const user = req.body;
      res.status(200).send({
        user,
        token: jwt.jwtSignUser(user)
      });
    } catch (err) {
      res.status(500).send({
        error: 'Произошла ошибка при попытке войти'
      })
    }
  }
}
