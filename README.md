# go to project dir Ui
cd Ui/
# install dependencies
npm install
# start project
npm start

# go to project dir Server
cd Sever/
# install dependencies
npm install
# start project
npm start